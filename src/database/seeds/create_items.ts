import { Knex } from 'knex'

export async function seed(knex: Knex) {
    await knex('items').insert([
        { title: "Papeis e papelao", image: "papel.png"},
        { title: "Vidros e lampadas", image: "vidro.png"},
        { title: "Oleo de cozinha", image: "oleo.png"},
        { title: "Residuos organicos", image: "organico.png"},
        { title: "Baterias e pilhas", image: "bateria.png"},
        { title: "Eletronicos", image: "eletronico.png"}
    ])
}