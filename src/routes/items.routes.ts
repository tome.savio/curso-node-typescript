import { Router } from 'express'

import knex from '../database/connection'

const itemsRouter = Router()

itemsRouter.get('/', async(request, response) => {
    const items = await knex('items').select('*');
    
    const serializedItems = items.map(items => {
        return{
        id: items.id,
        title: items.title,
        image_url: `http://localhost:3333/uploads/${items.image}`
        }
    })
    return response.json(serializedItems)
} )

export default itemsRouter;