import   Router, { request, response } from 'express';
import knex from '../database/connection';

const locationsRouter = Router();

locationsRouter.post('/', async (request, response) => { 
    
    const {
        name,
        email,
        whatsapp,
        latitude,
        longitude,
        city,
        uf,
        items
    } = request.body;

    const newLocation = {
        name, //cada variavel irá receber seu valor vindo da request
        image: "image.png",
        email,
        whatsapp,
        latitude,
        longitude,
        city,
        uf
    }

    const transaction = await knex.transaction()

    const newIds = await transaction('locations').insert(newLocation);
    
    const location_id = newIds[0] //retorna id do novo registro inserido

    const locationItems = items.map((item_id: number) => { //contrucao do registro referente a tabela location_items
        /*const selectedItem =   await transaction('items').where('id', item_id).first() //verifica se o item_id existe no banco
            
            if(!selectedItem){
                return response.status(400).json({message: 'Item not found'})
            }*/

        return{
            item_id,
            location_id
        }
    })
    
    await transaction('location_items').insert(locationItems)
    
    await transaction.commit()

    return response.json({
        id: location_id,
        ...newLocation // comando para passar todas as informaçoes do objeto sem referenciar cada atributo
    })

    response.json(newLocation)
} )

locationsRouter.get('/:id', async (request, response)=>{
    const { id } = request.params

    const location = await knex('locations').where('id', id).first()

    if(!location){
        return response.status(400).json({message: 'Location not foud!'})
    }

    const items = await knex('items')
        .join('location_items', 'items.id', '=', 'location_items.item_id')
        .where('location_items.location_id',id)
        .select('items.title')
    
    return response.json({location, items})

})

locationsRouter.get('/', async (request, response)=>{
    const { city, uf,items } = request.query

    const parsedItems = <any> String(items).split(',').map(item => Number(item.trim()))

    const locations = await knex('locations')
        .join('location_items', 'locations.id', '=', 'location_items.location_id')
        .whereIn('location_items.item_id', parsedItems)
        .where('city', String(city))
        .where('uf', String(uf))
        .distinct()
        .select('locations.*')
        

    return response.json(locations)

})

export default locationsRouter